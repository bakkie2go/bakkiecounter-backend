# Bakkiecounter Backend

This repository hosts the code of a simple hobby project to track one's coffee consumption.
If you like this idea, then either clone the repository and host it yourself, or use our public instance on [bakkiecounter.com](https://bakkiecounter.com). The frontend can be found [here](https://codeberg.org/bakkie2go/bakkiecounter-frontend).

Software stack:

* [Sanic](https://sanic.dev) as the backend
* [Tortoise-ORM](https://tortoise-orm.readthedocs.io/)
* [PostgreSQL](https://www.postgresql.org/) as the database system


## Getting started

The documentation can be found on [docs.bakkiecounter.com](https://docs.bakkiecounter.com)


## API

The following excerpt is copied from [bakkiecounter.com/api](https://bakkiecounter.com/api/):

```
================================================================================
  Bakkiecounter API
================================================================================

Welcome curious nerd! Here you can find some little documentation about the API.
Feel free to develop your own clients based on this service. Please do not
hesitate to contanct one of the developers. We appreciate bug reports and would
love to receive feature requests.


================================================================================
  API Methods
================================================================================

POST    bakkiecounter.com/api/auth/login
        request : {
            name : <User name>,
            pw   : <Password>,
        }

    User login

        response: A cookie that contains a JWT: {
            // User data
            ...
        }


POST    bakkiecounter.com/api/auth/logout

    You guessed it: user logout
    The user will be redirected to the front page of the domain,
    e.g. bakkiecounter.com
    The cookie containing the JWT will be deleted.


DELETE  bakkiecounter.com/api/users/
        Deletes user based on user inside of JWT


POST    bakkiecounter.com/api/users/
        request : {
            name : <User name>,
            mail : <Mail address> [Optional],
            pw   : <Password>
        }

    Create a new User account for the API. Required are the fields 'name' and 'pw'.
    The mail address is an optional field.

        response : {
            // User data
            ...
        }

GET     bakkiecounter.com/api/users/<user_id>

    Query the data of a specific user.

        response : {
            // User data
            ...
        }

GET     bakkiecounter.com/api/users/<user_id>/entries

    Query a list of the user's counter records.
    query their own user data.

        response : [
            {
                id : <Entry id>,
                user_id : <User id>,
                ts : <timestamp>,
            },
            ...
        ]

        Optionally you can specify pagination:
GET     bakkiecounter.com/api/users/<user_id>/entries/?page=<x>&paginate_by=<y>

POST    bakkiecounter.com/api/users/<user_id>/entries

    Create a new counter record for a specific user. As always, non-privileged
    users can not alter any data of user accounts others than themselves.

        response : {
            // Entry data
            ...
        }

GET     bakkiecounter.com/api/users/<user_id>/entries/count

    Query the number entries that a user has. In the future, optional filter
    parameters can be used to filter on given timeranges.

        response : number of entries

GET     bakkiecounter.com/api/users/<user_id>/entries/<entry_id>

    Query a specific counter record. Non-priviliged users can only query their
    own entries.

        response : {
            // Entry data
            ...
        }

DELETE  bakkiecounter.com/api/users/<user_id>/entries/<entry_id>

    Delete a specific entry from the user's counter records.

        response : {
            // Entry data
            ...
        }

================================================================================
  Thank you!
================================================================================

Lastly, thank you for your interest and enjoy the API!
```

## TODO

There are still plenty things to do to increase usability, user experience, additional features or documentation.
We try to keep track of it here in the README.



**API:**

* ~~/auth/logout gives a 401~~


**Backend:**

* ~~Add a proper default config file~~
* ~~Enable specific config as command line argument~~
* Specify common configuration parameters as args


**Database:**

* ~~Persistent sessions (not just cached)~~


**Developer Experience:**

* Setup script
* Deployment script
* Deployment pipeline and/or auto-update on hosted services
* ~~Systemd service files~~
* ~~Pip setuptools (requirements cherrypy, pscopg2, toml)~~


**Frontend:**

* Vue router history redirection


**User Experience:**

* ~~Cookie & Privacy Policy~~
* ~~Show username & profile information~~
* ~~Password changes~~
* Account deletion
* ~~When logged in, logout & login with other account fails (500)~~
* ~~Adding a user account that exists gives unclear error message~~
* ~~Generic error messages should be more helpful to user~~

