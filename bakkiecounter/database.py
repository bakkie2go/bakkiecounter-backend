from tortoise import Model, fields


class BaseModel(Model):
    """Base model for all following models"""
    id = fields.IntField(pk=True)

    class Meta:
        abstract = True


class User(BaseModel):
    mail = fields.CharField(max_length=255,)
    name = fields.CharField(max_length=255, unique=True)
    active = fields.BooleanField(null=False, default=True)


class Auth(BaseModel):
    user = fields.ForeignKeyField('models.User', related_name="auth",
                                  unique=True)
    pw_hash = fields.BinaryField()


class Entry(BaseModel):
    user = fields.ForeignKeyField('models.User', backref="entries")
    ts = fields.DatetimeField(auto_now=True)
