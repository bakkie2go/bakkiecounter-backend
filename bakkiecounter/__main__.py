#!/usr/bin/python3
from tortoise.contrib.sanic import register_tortoise
from sanic.log import LOGGING_CONFIG_DEFAULTS
from sanic.log import logger
from sanic import Sanic
import logging.config
import argparse
import os
import yacf

from .utils import shutdown
from bakkiecounter import config
from bakkiecounter.api.api import API
from bakkiecounter.api.auth import auth
from bakkiecounter.api.entry import Entry, EntryCount
from bakkiecounter.api.user import User, UserId

version_info = (0, 0, 0)
version = ".".join(str(c) for c in version_info)


app = Sanic("Bakkiecounter-backend")


def _load_config(path: str) -> yacf.Configuration:
    # Init config file if required
    if not os.path.isfile(path):
        config.init(path)
        logger.info(f"Initialized configuration file {path}.")
        logger.info("Please configure and restart the server.")
        shutdown(0)

    # Load configuration
    cfg = yacf.Configuration(config.DEFAULT, path)
    try:
        cfg.load()
    except Exception:
        logger.exception(
            f"Failed to load configuration. Check configuration file {path}"
        )
        shutdown(1)

    logger.info(f"Successfully loaded configuration from {path}.")

    return cfg


def _parse_args():
    parser = argparse.ArgumentParser(
        description="Startup an instance of a bakkiecounter service"
    )

    parser.add_argument(
        "-c",
        "--config",
        type=str,
        default=config.PATH,
        help="Path to a configuration file",
    )

    parser.add_argument(
        "-m",
        "--migrate",
        action="store_true",
        default=False,
        help="Migrates or initializes the database.",
    )

    parser.add_argument(
        "--version",
        action="version",
        version="%(prog)s {}".format(version),
        help="show the version number and exit",
    )

    return parser.parse_args()


def _prepare_logging(access, error, debug: bool):
    """Ensures logging will work flawlessly, e.g. creates logging directories
    and files and configures sanic to run in debug mode, if configured.

    :param access_file: Path to the access log file
    :param error_file: Path to the error log file
    :param debug: Debug flag, to ensure sanic will log in debug mode
    """
    # Create log directories
    for f in [access, error]:
        try:
            os.makedirs(os.path.dirname(f), exist_ok=True)
            if not os.path.isfile(f):
                with open(f, "w+") as _f:
                    _f.write("")
        except PermissionError:
            logger.exception(f"Insufficient permissions for {f}. "
                             "Please make sure the directory exists and "
                             "execute the following in the shell: \n"
                             f"# chown -R 'user' {access}"
                             f"# chown -R 'user' {error}"
                             f"# touch {f} \n"
                             f"# chown 'user' {f}")
            shutdown(1)

    access_file = {
        "class": "logging.handlers.RotatingFileHandler",
        "formatter": "access",
        "level": "INFO",
        "maxBytes": 1000000,
        "backupCount": 5,
        "filename": access
    }
    error_file = {
        "class": "logging.handlers.RotatingFileHandler",
        "formatter": "generic",
        "level": "ERROR",
        "maxBytes": 1000000,
        "backupCount": 5,
        "filename": error
    }

    LOGGING_CONFIG_DEFAULTS["handlers"]["access_file"] = access_file
    LOGGING_CONFIG_DEFAULTS["handlers"]["error_file"] = error_file
    LOGGING_CONFIG_DEFAULTS["loggers"]["sanic.access"]["handlers"] += \
                                      ["access_file"]
    LOGGING_CONFIG_DEFAULTS["loggers"]["sanic.root"]["handlers"] +=\
                                      ["error_file"]
    logging.config.dictConfig(LOGGING_CONFIG_DEFAULTS)
    logger.info(f"Access Log: {access}")
    logger.info(f"Error Log: {error}")
    if debug:
        logger.info("Debug mode activated; Log will be printed to stdout")


def main():
    args = _parse_args()
    cfg = _load_config(args.config)

    # Read logger information
    access_f = cfg.get("log.access")
    error_f = cfg.get("log.error")
    _debug = cfg.get("log.debug")

    _prepare_logging(access_f, error_f, _debug)

    # Begin with server startup sequence
    logger.info("Starting server...")

    # Connect to database
    logger.info(
        f"Connect to database {cfg.get('database.name')} "
        f"({cfg.get('database.host')}:{cfg.get('database.port')})"
    )

    # TODO: Properly migrate, instead of just creating tables
    logger.info("Database will be initialized after connecting to it.")
    register_tortoise(
            app,
            db_url=(f"postgres://{cfg.get('database.user')}:"
                    f"{cfg.get('database.password')}@"
                    f"{cfg.get('database.host')}:{cfg.get('database.port')}"
                    f"/{cfg.get('database.name')}"),
            modules={"models": ["bakkiecounter.database"]},
            generate_schemas=args.migrate
    )
    logger.info("Successfully connected to database.")

    # TODO: Add 301 - permanent redirect to actual domain

    app.add_route(API.as_view(), "/api")
    logger.info("Successfully mounted /api")

    app.add_route(UserId.as_view(), "/api/userid/")
    app.add_route(User.as_view(), "/api/users/<user_id>")
    logger.info("Successfully mounted /api/users/<user_id>")

    app.blueprint(auth)
    logger.info("Successfully mounted /api/auth")

    app.add_route(Entry.as_view(), "/api/users/<user_id>/entries/<entry_id>")
    logger.info("Successfully mounted /api/users/<user_id>/entries/<entry_id>")

    app.blueprint(EntryCount)
    logger.info("Successfully mounted /api/users/<user_id>/entries/count")

    logger.info("Started server successfully.")
    # Set JWT secret
    app.config.SECRET = cfg.get('general.secret_jwt_token')
    app.config.domain = cfg.get('general.domain')
    app.config.dev = cfg.get('general.dev')
    app.run(host=cfg.get('web.host'), port=cfg.get('web.port'),
            debug=cfg.get('general.dev'), auto_reload=True, access_log=True)


__name__ == "__main__" and main()
