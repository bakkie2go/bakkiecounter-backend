from passlib.hash import pbkdf2_sha512
from passlib.ifc import PasswordHash
from datetime import datetime, timedelta, timezone
import jwt
from sanic.log import logger
from sanic.response import empty
from sanic.exceptions import SanicException
from . import database as db


_401 = SanicException("Invalid combination of username and password.",
                      status_code=401)
_403 = SanicException("User account is blocked.", status_code=403)


def _404(message):
    return SanicException(f"{message} does not exist", status_code=404)


def represents_int(*args):
    """ Checks whether all arguments are integers.
    Useful for validating data in the backend.
    :param s: arguments
    :returns: whether all arguments are integers
    """
    for s in args:
        if s is None:
            return False
        try:
            int(s)
        except ValueError:
            return False
    return True


def paginate(page, paginate_by):
    """ This function takes a page and paginate_by and turns it into a limit
    and offset that the database can work with.
    :param page: the page number
    :param paginate_by: the number to paginate results by
    :returns: offset and limit tuple
    """
    paginate_by = int(paginate_by)
    page = int(page)
    return (page - 1) * paginate_by, paginate_by


async def jwt_encode(request, response=empty()):
    """returns a cookie containing a JWT,
    to be used after successfull login or signup.
    :request: request object
    :returns: a cookie containing a JWT

    """
    req = request.json
    domain = request.app.config.domain
    user_id = await db.User.filter(name=req["name"]).values("id")
    now = datetime.now(timezone.utc)
    expires = now + timedelta(days=1)
    message = {
        'iss': domain,
        'sub': user_id[0]["id"],
        'iat': now,
        'exp': expires
    }
    token = jwt.encode(message, request.app.config.SECRET)
    response.cookies["access_token"] = token
    response.cookies["access_token"]["domain"] = "." + domain
    response.cookies["access_token"]["httponly"] = True
    response.cookies["access_token"]["max-age"] = 86400
    response.cookies["access_token"]["expires"] = expires
    response.cookies["access_token"]["samesite"] = "Strict"
    response.cookies["access_token"]["secure"] = True
    return response


async def jwt_decode(request):
    """Returns the user id in the JWT"""
    return jwt.decode(request.cookies.get("access_token"),
                      request.app.config.SECRET,
                      algorithms=["HS256"])["sub"]


def shutdown(code=0):
    logger.info("Shutdown server")
    exit(code)


def _alg() -> PasswordHash:
    """Returns a new instance of the hash
    algorithm with constant parameters."""
    return pbkdf2_sha512.using(salt_size=128, rounds=100000)


# TODO: Consider security.salt from config
def hash(secret) -> str:
    """Hashes a secret string, e.g. a password

    :param secret: The secret to hash
    :return: hashed string
    """
    try:
        return _alg().hash(secret)
    except (TypeError, ValueError) as e:
        logger.error("Failed to hash a secret.")
        logger.error(e)
        raise SanicException("Failed to hash a secret", status_code=500)


def verify_hash(secret, _hash) -> bool:
    """Verifies a password against a hash that is stored in the database.

    :param secret: The secret string to verify against a hash
    :param _hash: The hash that will be used as verifier
    :return: True, if verification succedes, otherwise false
    """
    try:
        return _alg().verify(secret, _hash)
    except (TypeError, ValueError) as e:
        logger.error("Failed to verify a secret to a hashed value")
        logger.error(e)
        raise SanicException("Failed to verify a secret to a hashed value",
                             status_code=500)
