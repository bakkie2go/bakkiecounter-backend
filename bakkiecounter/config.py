import toml
import random
import string

s = []
s.extend(list(string.ascii_lowercase))
s.extend(list(string.ascii_uppercase))
s.extend(list(string.digits))
s.extend(list(string.punctuation))


PATH = "config.toml"

DEFAULT = {
    "general": {
        "dev": False,
        "secret_jwt_token": "".join(random.sample(s, 64)),
        "domain": "foo.com"
    },
    "database": {
        "host": "localhost",
        "port": 5432,
        "name": "name",
        "user": "postgres",
        "password": "secret",
    },
    "log": {
        "access": "/var/log/bakkiecounter/access.log",
        "error": "/var/log/bakkiecounter/error.log",
    },
    "web": {
        "host": "0.0.0.0",
        "port": 4885,
    }
}


def init(f: str):
    """Initializes a configuration file with the default parameters

    :param f: Path of new configuration file
    """
    with open(f, "w") as _f:
        toml.dump(DEFAULT, _f)
