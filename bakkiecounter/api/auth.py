from functools import wraps
from sanic import Blueprint
from sanic.log import logger
from sanic.response import empty, json
from sanic.exceptions import SanicException
from datetime import datetime, timedelta, timezone
import jwt
from jwt.exceptions import InvalidTokenError
from bakkiecounter import database as db
from bakkiecounter import utils
from bakkiecounter.utils import _401, _403

auth = Blueprint("auth")


@auth.post("/api/auth/login")
async def login(request):
    """User login

    :param from_page: possibly a page redirection
    :return: Dictionary with UID and a possibly non-empty error string
    """
    req = request.json

    if req["name"] is None or req["pw"] is None:
        raise SanicException("Illegal username or password.", status_code=400)

    # Check authentication
    user = await authenticate(req["name"], req["pw"])
    return await utils.jwt_encode(request, response=json(user))


@auth.post("/api/auth/logout")
async def logout(request):
    """User logout
    :return cookie deletion response:
    """

    # TODO proper cookie deletion instead of rewrite
    # https://sanicframework.org/en/guide/basics/cookies.html#deleting
    # TODO redirect to front page
    # response = redirect("/")
    response = empty()
    domain = request.app.config.domain
    now = datetime.now(timezone.utc)
    expires = now + timedelta(seconds=1)
    response.cookies["access_token"] = ""
    response.cookies["access_token"]["domain"] = "." + domain
    response.cookies["access_token"]["httponly"] = True
    response.cookies["access_token"]["max-age"] = 1
    response.cookies["access_token"]["expires"] = expires
    response.cookies["access_token"]["samesite"] = "Strict"
    response.cookies["access_token"]["secure"] = True
    return response


async def authenticate(username: str, pw: str) -> dict:
    """Checks credentials of a user and authenticates the user.
    :param username: User name
    :param pw: password
    :return: Returns the user record of the database.
    """
    user = await db.User.filter(name=username).values()
    if len(user) < 1:
        raise _401
    user = user[0]
    if not user["active"]:
        raise _403

    auth = await db.Auth.filter(user_id=user["id"]).values()
    if not utils.verify_hash(pw, auth[0]["pw_hash"]):
        raise _401
    return user


async def update_auth_details(user_id: int, pw: str):
    """Updates authentication details of a user
    :param user_id: User id
    :param pw: The user's password
    """
    pw_hash = utils.hash(pw)

    try:
        await db.Auth.filter(id=user_id).update(pw_hash=pw_hash)
    except Exception as e:
        logger.error(e)


def check_token(request) -> str:
    if not request.cookies.get("access_token"):
        return "-1"
    try:
        user_id = jwt.decode(
           request.cookies.get("access_token"),
           request.app.config.SECRET,
           algorithms=["HS256"]
        )
    except InvalidTokenError:
        return "-1"
    else:
        return str(user_id["sub"])


def protected(wrapped):
    def decorator(f):
        @wraps(f)
        async def decorated_function(request, *args, **kwargs):
            user_id = check_token(request)
            if user_id != "-1" and user_id == str(kwargs["user_id"]):
                response = await f(request, *args, **kwargs)
                return response
            else:
                raise SanicException("Unauthorized or not logged in",
                                     status_code=401)
        return decorated_function
    return decorator(wrapped)
