import re
from sanic.response import json, text
from sanic.views import HTTPMethodView
from sanic.exceptions import SanicException
from bakkiecounter import database as db
from bakkiecounter import utils
from bakkiecounter.api.auth import protected, check_token
from bakkiecounter.utils import _404, jwt_encode


class UserId(HTTPMethodView):
    async def get(self, request):
        uid = check_token(request)
        if uid != "-1":
            return text(uid)
        else:
            raise SanicException("Unauthorized or not logged in",
                                 status_code=401)


class User(HTTPMethodView):
    @staticmethod
    @protected
    async def get(request, user_id: int):
        user = await db.User.filter(id=user_id).values()
        if len(user) < 1:
            raise _404(f"User id {user_id}")
        return json(user[0])

    async def post(self, request, user_id: int):
        req = request.json

        mail = "" if req.get("mail") is None else req.get("mail")
        name = req["name"]
        pw = req["pw"]

        # validate input
        if not name or not pw:
            raise SanicException(
                "Requires both parameters of username and password to be set.",
                status_code=400,
            )

        # Hash password
        pw_hash = utils.hash(req["pw"])
        req["pw"] = ""

        # Ensure no user with same name exists
        u = await db.User.filter(name=name).values()
        if len(u) > 0:
            raise SanicException("Username already in use", status_code=409)
        # Ensure no user with email name exists
        if mail != "":
            u = await db.User.filter(mail=mail).values("mail")
            if len(u) > 0:
                raise SanicException("Email already in use", status_code=409)

        # Insert user
        await db.User(name=name, mail=mail, active=True).save()
        user = await db.User.filter(name=name).values()

        # Insert auth
        await db.Auth(user_id=user[0]["id"],
                      pw_hash=bytes(pw_hash, 'utf8')).save()
        return await jwt_encode(request=request, response=json(user[0]))

    @staticmethod
    @protected
    async def put(request, user_id: int):
        req = request.json
        # Hash password
        db_pw_hash = (await db.Auth.filter(user_id=user_id).values("pw_hash"))
        if not utils.verify_hash(req["current_pw"], db_pw_hash[0]["pw_hash"]):
            return text("Current password incorrect.", status=400)
        req["current_pw"] = ""
        if req["new_pw"] != "":
            new_pw_hash = utils.hash(req["new_pw"])
            req["new_pw"] = ""
            # TODO .update() throws error.
            await db.Auth.filter(user_id=user_id).delete()
            (await db.Auth(user_id=user_id,
             pw_hash=bytes(new_pw_hash, 'utf8')).save())
        name = req["name"]
        if name != "":
            if len(name) > 1024:
                return text("Username too long.", status=400)
            # Ensure no user with same name exists
            u = await db.User.filter(name=name).values()
            if len(u) > 0:
                # Obviously, if the submitted username is the same as
                # the user's current username, do not throw this error
                current_name = (await db.User.filter(id=user_id)
                                .values("name"))
                if current_name[0]["name"] != name:
                    return text("Username is already in use.", status=409)
            await db.User.filter(id=user_id).update(name=name)
        mail = req["mail"]
        if mail != "" and not req["remove_email"]:
            if not re.match(r".+@.+\..+", mail):
                return text("Invalid email.", status=400)
            # Ensure no user with email name exists
            u = await db.User.filter(mail=mail).values("mail")
            if len(u) > 0:
                return text("Email already in use.", status=409)
            await db.User.filter(id=user_id).update(mail=mail)
        if req["remove_email"]:
            await db.User.filter(id=user_id).update(mail="")
        new_user = (await db.User.filter(id=user_id).values())
        return json(new_user[0])

    @staticmethod
    @protected
    async def delete(request, user_id: int):
        user = await db.User.filter(id=user_id).values()
        if len(user) < 1:
            raise _404(f"User id {user_id}")
        await db.Entry.filter(user_id=user_id).delete()
        await db.Auth.filter(user_id=user_id).delete()
        await db.User.filter(id=user_id).delete()
        return text("User deleted successfully")
