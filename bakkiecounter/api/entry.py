from datetime import datetime, timezone
from sanic.views import HTTPMethodView
from sanic.response import json, text
from sanic.exceptions import SanicException
from sanic import Blueprint
from bakkiecounter import database as db
from bakkiecounter.api.auth import protected
from bakkiecounter.utils import _404, represents_int, paginate
from math import ceil


EntryCount = Blueprint("EntryCount")


@EntryCount.get("/api/users/<user_id>/entries/count")
@protected
async def count(request, user_id: int):
    count = await db.Entry.filter(user_id=user_id).count()
    return text(str(count))


class Entry(HTTPMethodView):
    decorators = [protected]

    async def delete(self, request, user_id: int, entry_id: int):
        entry = await db.Entry.filter(id=entry_id).values()
        if len(entry) < 1:
            raise _404(f"Entry {entry_id}")
        if not entry[0]["user_id"] == int(user_id):
            raise SanicException(
                "Forbidden: Can not access entries of other users.",
                status_code=403
            )
        await db.Entry.filter(id=entry_id).delete()
        entry = entry[0]
        return json(entry)

    async def get(self, request, user_id: int, entry_id: int):
        if entry_id == "":
            page = request.args.get('page')
            paginate_by = request.args.get('paginate_by')
            if represents_int(page, paginate_by):
                offset, limit = paginate(page, paginate_by)
                entries = (await db.Entry.filter(user_id=user_id)
                           .order_by("-id").offset(offset).limit(limit)
                           .values())
                totalPages = await db.Entry.filter(user_id=user_id).count()
                totalPages = ceil(int(totalPages) / int(paginate_by))
                entries = await self.serialise(entries)
                return json({
                    "totalPages": totalPages,
                    "entries": entries
                })
            else:
                entries = (await db.Entry.filter(user_id=user_id)
                           .order_by("-id").values())
        else:
            entries = await (db.Entry.filter(user_id=user_id, id=entry_id)
                             .order_by("-id").values())
        entries = await self.serialise(entries)
        return json(entries)

    async def post(self, request, user_id: int, entry_id: int):
        await db.Entry(user_id=user_id, ts=datetime.utcnow()).save()
        entry = await db.Entry.filter(user_id=user_id)\
            .order_by("-ts").first().values()
        entry["ts"] = str(entry["ts"])
        return json(entry)

    async def serialise(self, x):
        """Serializes an array of transactions by serializing the timestamp
        :x: Array
        :returns: Serialized array
        """
        for t in x:
            t.update(ts=t["ts"].replace(tzinfo=timezone.utc).astimezone(tz=None).strftime("%d-%m-%Y %H:%M:%S"))
        return x
