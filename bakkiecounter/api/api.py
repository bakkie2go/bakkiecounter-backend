from sanic.response import text
from sanic.views import HTTPMethodView


class API(HTTPMethodView):
    async def get(self, request):
        return text("""
================================================================================
  Bakkiecounter API
================================================================================

Welcome curious nerd! Here you can find some little documentation about the API.
Feel free to develop your own clients based on this service. Please do not
hesitate to contanct one of the developers. We appreciate bug reports and would
love to receive feature requests.


================================================================================
  API Methods
================================================================================

POST    bakkiecounter.com/api/auth/login
        request : {
            name : <User name>,
            pw   : <Password>,
        }

    User login

        response: A cookie that contains a JWT: {
            // User data
            ...
        }


POST    bakkiecounter.com/api/auth/logout

    You guessed it: user logout
    The user will be redirected to the front page of the domain,
    e.g. bakkiecounter.com
    The cookie containing the JWT will be deleted.


DELETE  bakkiecounter.com/api/users/
        Deletes user based on user inside of JWT


POST    bakkiecounter.com/api/users/
        request : {
            name : <User name>,
            mail : <Mail address> [Optional],
            pw   : <Password>
        }

    Create a new User account for the API. Required are the fields 'name' and 'pw'.
    The mail address is an optional field.

        response : {
            // User data
            ...
        }

GET     bakkiecounter.com/api/users/<user_id>

    Query the data of a specific user.

        response : {
            // User data
            ...
        }

GET     bakkiecounter.com/api/users/<user_id>/entries

    Query a list of the user's counter records.
    query their own user data.

        response : [
            {
                id : <Entry id>,
                user_id : <User id>,
                ts : <timestamp>,
            },
            ...
        ]

        Optionally you can specify pagination:
GET     bakkiecounter.com/api/users/<user_id>/entries/?page=<x>&paginate_by=<y>

POST    bakkiecounter.com/api/users/<user_id>/entries

    Create a new counter record for a specific user. As always, non-privileged
    users can not alter any data of user accounts others than themselves.

        response : {
            // Entry data
            ...
        }

GET     bakkiecounter.com/api/users/<user_id>/entries/count

    Query the number entries that a user has. In the future, optional filter
    parameters can be used to filter on given timeranges.

        response : number of entries

GET     bakkiecounter.com/api/users/<user_id>/entries/<entry_id>

    Query a specific counter record. Non-priviliged users can only query their
    own entries.

        response : {
            // Entry data
            ...
        }

DELETE  bakkiecounter.com/api/users/<user_id>/entries/<entry_id>

    Delete a specific entry from the user's counter records.

        response : {
            // Entry data
            ...
        }

================================================================================
  Thank you!
================================================================================

Lastly, thank you for your interest and enjoy the API!

""")
